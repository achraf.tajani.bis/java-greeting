import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ApiError, ErreurService } from 'src/app/services';



@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router, private errorService: ErreurService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error.error instanceof ErrorEvent) {
          // client-side error
          console.error(JSON.stringify(error));
          return throwError(error);
        } else {
          // server-side error
          if ([200, 201, 202, 400].indexOf(error.status) !== -1) {
            // Bad request
            return throwError(error);
          }
          if ([500].indexOf(error.status ) > -1 && (
            error.error.message.indexOf('400 :') > -1 ) ) {
            return throwError(error);
          }
          const apiError = new ApiError();
          apiError.status = error.status;
          apiError.statusText = error.statusText;
          if (typeof error.error === 'string') {
            apiError.message = error.error;
          } else if (error.error.errorCode) {
            apiError.message = error.error.message;
          }
          this.errorService.putApiError(apiError);
          this.router.navigate(['/error'], { skipLocationChange: true });
          return throwError(error);
        }
      }) 
    );
  }
}
