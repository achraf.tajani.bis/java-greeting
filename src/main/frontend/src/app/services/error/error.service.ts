import { Injectable } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';

export class ApiError {
  status!: number;
  statusText!: string;
  message!: string;
  errorCode!: number;
  errors: any;
}

@Injectable({
  providedIn: 'root',
})
export class ErreurService {
  private apiErrorState = new ReplaySubject<ApiError | null>(1);

  putApiError(error: ApiError): void {
    this.apiErrorState.next(error);
  }

  error(): Observable<ApiError | null> {
    return this.apiErrorState.asObservable();
  }
}
