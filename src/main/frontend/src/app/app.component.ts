import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
interface GreetingModel {
  id:number;
  content:string;
 }
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  showFiller = false;
  greeting?:GreetingModel
  constructor( private httpclient: HttpClient) {}
  ngOnInit(): void {
    const url ='/api/greetings';
    this.httpclient.get(url).subscribe((d:any) =>  {
      this.greeting = d;
    });
  }
  title = 'galrapport-v1';
}
