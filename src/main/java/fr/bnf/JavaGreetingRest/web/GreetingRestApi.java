package fr.bnf.JavaGreetingRest.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import fr.bnf.JavaGreetingRest.entities.Greeting;



@RestController
@RequestMapping(path = "/api")
public class GreetingRestApi {

	
	@GetMapping(path="/greeting")
	public String greeting() {
		return "Hello";
	}
	
	@GetMapping(path="/greeting/{param}")
	public String greeting(@PathVariable(name="param") String param){
		return "Hello " + param;
	}
	
	@GetMapping(path="/greetings")
	public Greeting greetings() {
		Greeting g = new Greeting(1, "Hello World !");
		return g;
	}
}


