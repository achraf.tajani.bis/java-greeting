package fr.bnf.JavaGreetingRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaGreetingRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaGreetingRestApplication.class, args);
	}

}
